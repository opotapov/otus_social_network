require 'sinatra/base'
require "sinatra/reloader"

require_relative '../application/handlers/registration_handler'
require_relative '../application/handlers/login_handler'
require_relative '../application/handlers/current_user_handler'
require_relative '../application/handlers/user_list_handler'
require_relative '../application/handlers/get_user_handler'
require_relative '../application/handlers/friend_request_handler'
require_relative '../application/handlers/pending_requests_handler'
require_relative '../application/handlers/change_friend_request_status_handler'
require_relative '../application/handlers/remove_friendship_handler'
require_relative '../application/handlers/search_users_handler'

module Web
  class SocialNetworkRouter < Sinatra::Base
    configure :production, :development do
      enable :logging
      enable :sessions
    end

    set :public_folder, File.dirname(__FILE__) + '/public'

    helpers do
      def gender(gender_num)
        ['Male', 'Female'][gender_num.to_i - 1]
      end
    end

    before do
      current_user_id = session[:user_id]
      logger.info(current_user_id)

      if current_user_id
        @current_user = CurrentUserHandler.call(current_user_id)
        logger.info(@current_user)
      end
    end

    get '/' do
      if params[:register] == 'ok'
        @message = 'User was created successfully!'
      end

      if params[:login] == 'ok'
        @message = 'Logged in successfully!'
      end

      if params[:friend_request] == 'ok'
        @message = 'Friend Request sent!'
      end

      @users = UserListHandler.call

      erb :index
    end

    get '/users/:id' do
      # @current_user = { 'id' => 10 }

      current_user_id = @current_user ? @current_user['id'] : nil

      result = GetUserHandler.call(params[:id], current_user_id)
      @user = result.user
      @interests = result.interests
      @friends = result.friends
      @is_friend = result.is_friend
      logger.info(current_user_id)
      logger.info(result)

      if @current_user && @current_user['id'] == @user['id']
        @pending_requests = PendingRequestsHandler.call(@user['id'])
        logger.info(@pending_requests)
      end

      erb :user
    end

    get '/register' do
      erb :register
    end

    get '/sign_in' do
      erb :sign_in
    end

    post '/register' do
      result = RegistrationHandler.call(
        email: params[:email],
        password: params[:password],
        password_confirmation: params[:password_confirmation],
        first_name: params[:first_name],
        last_name:  params[:last_name],
        age:        params[:age],
        interests:  params[:interests],
        city:       params[:city],
        gender:     params[:gender],
      )

      logger.info(result)

      if result.status == :ok
        redirect to('/?register=ok')
      else
        @errors = result.errors
        erb :register
      end
    end

    post '/sign_in' do
      result = LoginHandler.call(
        email: params[:email],
        password: params[:password],
      )

      logger.info(result)

      if result.entity
        session[:user_id] = result.entity['id']
        @current_user = result.entity
        redirect to('/?login=ok')
      else
        @error = 'Email or password is incorrect!'
        erb :sign_in
      end
    end

    post '/sign_out' do
      session[:user_id] = nil
      @current_user = nil
      @message = 'Logged out successfully!'
      redirect to('/sign_in')
    end

    post '/friend_request' do
      result = FriendRequestHandler.call(
        current_user_id: @current_user['id'],
        user_id:         params['user_id']
      )

      if result.status == :ok
        redirect to('/?friend_request=ok')
      else
        redirect to('/')
      end
    end

    post '/friend_requests/:id/accept' do
      result = ChangeFriendRequestStatusHandler.accept(
        current_user_id: @current_user['id'],
        id:              params['id']
      )

      redirect to("/users/#{@current_user['id']}")
    end

    post '/friend_requests/:id/reject' do
      result = ChangeFriendRequestStatusHandler.reject(
        current_user_id: @current_user['id'],
        id:              params['id']
      )

      redirect to("/users/#{@current_user['id']}")
    end

    post '/friendships/:user_id/delete' do
      result = RemoveFriendshipHandler.call(
        current_user_id: @current_user['id'],
        user_id:         params['user_id']
      )

      redirect to("/users/#{@current_user['id']}")
    end

    get '/search' do
      @users = SearchUsersHandler.call(
        first_name_prefix: params['first_name'],
        last_name_prefix:  params['last_name']
      )

      erb :search
    end
  end
end
