require 'mysql2'

class DBClient
  def self.client
    @@client ||= get_client
  end

  def self.get_client
    if ENV["APP_ENV"] == 'production'

      url = URI.parse(ENV["CLEARDB_DATABASE_URL"])

      Mysql2::Client.new(
        host: url.host,
        username: url.user,
        password: url.password,
        database: url.path[1..-1]
      )
    else
      Mysql2::Client.new(
        host:     "0.0.0.0",
        port:     "3308",
        username: "root",
        password: 'example',
        database: 'otus_social_network'
      )
    end
  end

  def self.db_client
    if ENV["APP_ENV"] == 'production'
      url = URI.parse(ENV["CLEARDB_DATABASE_URL"])

      Mysql2::Client.new(
        host: url.host,
        username: url.user,
        password: url.password,
        database: url.path[1..-1]
      )
    else
      Mysql2::Client.new(
        host:     "localhost",
        username: "root",
        password: 'pass_mysql',
        database: 'otus_social_network'
      )
    end
  end
end