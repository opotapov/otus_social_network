-- Run on Master
reset master;

CREATE USER IF NOT EXISTS repl_user IDENTIFIED BY 'example';
GRANT REPLICATION SLAVE ON *.* TO 'repl_user'@'%' WITH GRANT OPTION;
