require_relative '../db_client'

# DBClient.client.query('DROP DATABASE IF EXISTS otus_social_network;')
# DBClient.client.query('CREATE DATABASE otus_social_network;')

# DBClient.client.query('USE otus_social_network;')
DBClient.get_client.query('''
    CREATE TABLE users (
        id INT NOT NULL AUTO_INCREMENT,
        email CHAR(255),
        password_hash CHAR(128),
        first_name CHAR(128),
        last_name CHAR(128),
        age INT,
        gender INT,
        city CHAR(128),
        PRIMARY KEY(id)
    ) ENGINE=InnoDB;
    ''')

DBClient.get_client.query('''
    CREATE TABLE user_interests (
        id INT NOT NULL AUTO_INCREMENT,
        user_id INT,
        interest CHAR(128),
        PRIMARY KEY(id),
        FOREIGN KEY (user_id)
          REFERENCES users(id)
          ON DELETE CASCADE
    ) ENGINE=InnoDB;
    ''')
