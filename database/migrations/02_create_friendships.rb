require_relative '../db_client'

DBClient.get_client.query('''
    CREATE TABLE friend_requests (
        id INT NOT NULL AUTO_INCREMENT,
        user_id INT,
        target_id INT,
        status INT,
        PRIMARY KEY(id),
        FOREIGN KEY (user_id)
          REFERENCES users(id)
          ON DELETE CASCADE,
        FOREIGN KEY (target_id)
          REFERENCES users(id)
          ON DELETE CASCADE
    ) ENGINE=InnoDB;
    ''')

DBClient.get_client.query('''
    CREATE TABLE friendships (
        id INT NOT NULL AUTO_INCREMENT,
        user_id INT,
        friend_id INT,
        PRIMARY KEY(id),
        FOREIGN KEY (user_id)
          REFERENCES users(id)
          ON DELETE CASCADE,
        FOREIGN KEY (friend_id)
          REFERENCES users(id)
          ON DELETE CASCADE
    ) ENGINE=InnoDB;
    ''')
