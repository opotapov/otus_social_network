require 'faker'
require_relative '../../application/repository/users_repository'

1000.times do |batch|
  puts "batch #{batch+1} of 1000"

  1000.times do |n|
    UsersRepository.create(
      email:         Faker::Internet.email,
      password_hash: 'hash',
      first_name:    Faker::Name.first_name,
      last_name:     Faker::Name.last_name,
      age:           rand(10) + 10,
      gender:        rand(2),
      city:          Faker::Address.city
    )
  end
end