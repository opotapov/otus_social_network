mysqldump -h 127.0.0.1 -u root -p --no-data otus_social_network > schema.sql

docker exec -i otussocialnetwork_dbmaster_1 sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < ./schema.sql

docker exec -i otussocialnetwork_dbmaster_1 sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < ./database/replication/async/master.sql
docker exec -i otussocialnetwork_db_slave1_1 sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < ./database/replication/async/slave.sql
docker exec -i otussocialnetwork_db_slave2_1 sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < ./database/replication/async/slave.sql
