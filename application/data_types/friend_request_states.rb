require_relative '../repository/friend_requests_repository'
require_relative '../data_types/friend_request_states'

class FriendRequestStates
  PENDING  = 0
  ACCEPTED = 1
  REJECTED = 2
end