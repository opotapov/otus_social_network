require 'bcrypt'

class PasswordHashGenerator
  SALT = 'super_secret_salt'

  def self.call(password)
    BCrypt::Password.create(password + SALT)
  end

  def self.check_password(password_hash, password)
    BCrypt::Password.new(password_hash).is_password?(password + SALT)
  end
end