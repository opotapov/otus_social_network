require_relative '../repository/friend_requests_repository'
require_relative '../data_types/friend_request_states'

class FriendRequestHandler
  def self.call(current_user_id:, user_id:)
    FriendRequestsRepository.create(
      user_id:   current_user_id,
      target_id: user_id,
      status:    FriendRequestStates::PENDING
    )

    return OpenStruct.new(status: :ok)
  end
end