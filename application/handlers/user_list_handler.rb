require_relative '../repository/users_repository'

class UserListHandler
  def self.call()
    return UsersRepository.find_all
  end
end