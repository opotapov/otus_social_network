require_relative '../repository/friend_requests_repository'
require_relative '../data_types/friend_request_states'

class PendingRequestsHandler
  def self.call(user_id)
    FriendRequestsRepository.find_pending_for_target(user_id)
  end
end