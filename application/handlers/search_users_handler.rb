require_relative '../repository/users_repository'

class SearchUsersHandler
  def self.call(first_name_prefix:, last_name_prefix:)
    return UsersRepository.find_by_prefix(first_name_prefix, last_name_prefix)
  end
end