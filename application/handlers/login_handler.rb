require_relative '../repository/users_repository'
require_relative '../services/password_hash_generator'

class LoginHandler
  def self.call(email:, password:)
    user = UsersRepository.find_by_email(email)

    if user && PasswordHashGenerator.check_password(user['password_hash'], password)
      return OpenStruct.new(
        status: :ok,
        errors: [],
        entity: user
      )
    end

    return OpenStruct.new(
      status: :error,
      errors: [{ code: :login_error, message: 'Email or password is incorrect' }],
      entity: nil
    )
  end
end