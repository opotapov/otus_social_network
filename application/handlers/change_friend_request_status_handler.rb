require_relative '../repository/friend_requests_repository'
require_relative '../repository/friendships_repository'
require_relative '../data_types/friend_request_states'

class ChangeFriendRequestStatusHandler
  def self.accept(current_user_id:, id:)
    friend_request = FriendRequestsRepository.find_for_target(id, current_user_id)

    return if !friend_request

    FriendRequestsRepository.change_state(friend_request['id'], FriendRequestStates::ACCEPTED)
    FriendshipsRepository.create(friend_request['user_id'], friend_request['target_id'])
    FriendshipsRepository.create(friend_request['target_id'], friend_request['user_id'])
  end

  def self.reject(current_user_id:, id:)
    friend_request = FriendRequestsRepository.find_for_target(id, current_user_id)

    return if !friend_request

    FriendRequestsRepository.change_state(friend_request['id'], FriendRequestStates::REJECTED)
  end
end