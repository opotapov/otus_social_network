require_relative '../repository/users_repository'

class CurrentUserHandler
  def self.call(user_id)
    return UsersRepository.find_by_id(user_id)
  end
end