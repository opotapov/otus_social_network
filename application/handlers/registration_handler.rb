require_relative '../repository/users_repository'
require_relative '../repository/user_interests_repository'
require_relative '../services/password_hash_generator'

class RegistrationHandler
  class ValidationError < StandardError
    def initialize(code:, message:)
      @code = code
      @message = message
    end

    def code
      @code
    end

    def message
      @message
    end
  end

  def self.call(email:, password:, password_confirmation:, first_name:, last_name:,
    age:, gender:, interests:, city:)

    existing_user = UsersRepository.find_by_email(email)

    if existing_user
      raise ValidationError.new(code: :email_exists, message: 'Email already registered')
    end

    {
      email: email,
      password: password,
      password_confirmation: password_confirmation,
      first_name: first_name,
      last_name: last_name,
      age: age,
      gender: gender,
      city: city
    }.each do |k, v|
      unless v
        raise ValidationError.new(code: k, message: "#{key.capitalize!} is required!")
      end
    end

    if password != password_confirmation
      raise ValidationError.new(code: :password_confirmation, message: "Password confirmation is wrong")
    end

    UsersRepository.create(
      email:         email,
      password_hash: PasswordHashGenerator.call(password),
      first_name:    first_name,
      last_name:     last_name,
      age:           age,
      gender:        gender,
      city:          city
    )

    user = UsersRepository.find_by_email(email)

    interests.split(',').map(&:strip).each do |interest|
      UserInterestsRepository.create(
        user_id:  user['id'],
        interest: interest
      )
    end

    return OpenStruct.new(
      status: :ok,
      errors: [],
      entity: { id: user['id'] }
    )
  rescue ValidationError => e
    return OpenStruct.new(
      status: :error,
      errors: [{ code: e.code, message: e.message }]
    )
  rescue => e
    return OpenStruct.new(
      status: :error,
      errors: [{ code: :database_error, message: 'User was not created', error_message: e.message }]
    )
  end
end