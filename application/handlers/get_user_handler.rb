require_relative '../repository/users_repository'
require_relative '../repository/user_interests_repository'
require_relative '../repository/friendships_repository'

class GetUserHandler
  def self.call(user_id, current_user_id)
    is_friend = current_user_id ? FriendshipsRepository.exists?(user_id, current_user_id) : false

    return OpenStruct.new(
      user: UsersRepository.find_by_id(user_id),
      interests: UserInterestsRepository.find_by_user(user_id),
      friends: FriendshipsRepository.find_for_user(user_id),
      is_friend: is_friend
    )
  end
end