require_relative '../repository/friendships_repository'

class RemoveFriendshipHandler
  def self.call(current_user_id:, user_id:)
    FriendshipsRepository.delete(current_user_id, user_id)
    FriendshipsRepository.delete(user_id, current_user_id)
  end
end