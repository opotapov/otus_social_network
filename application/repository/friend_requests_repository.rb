require_relative '../../database/db_client'

class FriendRequestsRepository
  def self.create(user_id:, target_id:, status:)
    client = DBClient.client
    client.query("""
      INSERT INTO friend_requests
      (user_id, target_id, status)
      VALUES
      (#{user_id.to_i}, #{target_id.to_i}, #{status.to_i});
      """
    )
  end

  def self.find_pending_for_target(target_id)
    client = DBClient.client
    client.query("""
      SELECT
        f.id as id,
        u.first_name,
        u.last_name
      FROM friend_requests f
      INNER JOIN users u ON u.id = f.user_id
      WHERE target_id=#{target_id.to_i} AND status=0
      ORDER BY f.id LIMIT 100;
    """).to_a
  end

  def self.find_for_target(id, target_id)
    client = DBClient.client
    client.query("""
      SELECT * FROM friend_requests
      WHERE id=#{id.to_i} AND target_id=#{target_id.to_i}
      ORDER BY id LIMIT 1;
    """).first
  end

  def self.change_state(id, status)
    client = DBClient.client
    client.query("""
      UPDATE friend_requests
      SET status=#{status.to_i}
      WHERE id=#{id.to_i};
    """).to_a
  end
end