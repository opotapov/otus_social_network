require_relative '../../database/db_client'

class FriendshipsRepository
  def self.create(user_id, friend_id)
    client = DBClient.client
    client.query("""
      INSERT INTO friendships
      (user_id, friend_id)
      VALUES
      (#{user_id.to_i}, #{friend_id.to_i});
      """
    )
  end

  def self.delete(user_id, friend_id)
    client = DBClient.client
    client.query("""
      DELETE FROM friendships
      WHERE user_id=#{user_id.to_i} AND friend_id=#{friend_id.to_i};
    """).to_a
  end

   def self.find_for_user(user_id)
    client = DBClient.client
    client.query("""
      SELECT
        u.id as id,
        u.first_name,
        u.last_name
      FROM friendships f
      INNER JOIN users u ON u.id = f.friend_id
      WHERE user_id=#{user_id.to_i}
      ORDER BY id LIMIT 1;
    """).to_a
  end


  def self.exists?(user_id, friend_id)
    client = DBClient.client
    !!client.query("""
      SELECT * FROM friendships
      WHERE user_id=#{user_id.to_i} AND friend_id=#{friend_id.to_i}
      ORDER BY id LIMIT 1;
    """).first
  end
end