require_relative '../../database/db_client'

class UserInterestsRepository
  def self.find_by_user(user_id)
    client = DBClient.client
    client.query("SELECT * FROM user_interests WHERE user_id=#{client.escape(user_id)};").to_a
  end

  def self.create(user_id:, interest:)
    client = DBClient.client
    client.query("""
      INSERT INTO user_interests
      (user_id, interest)
      VALUES
      (#{user_id}, '#{client.escape(interest)}');
      """) rescue nil
  end
end