require_relative '../../database/db_client'

class UsersRepository
  def self.find_by_email(email)
    client = DBClient.client
    client.query("SELECT * FROM users WHERE email='#{client.escape(email)}' LIMIT 1").first
  end

  def self.find_by_id(id)
    client = DBClient.client
    client.query("SELECT * FROM users WHERE id=#{id.to_i} LIMIT 1").first
  end

  def self.create(email:, password_hash:, first_name:, last_name:, age:, gender:, city:)
    client = DBClient.client
    client.query("""
      INSERT INTO users
      (email, password_hash, first_name, last_name, age, gender, city)
      VALUES
      ('#{client.escape(email)}', '#{password_hash}', '#{client.escape(first_name)}', '#{client.escape(last_name)}', #{age.to_i}, #{gender.to_i}, '#{client.escape(city)}');
      """
    )
  end

  def self.find_all
    client = DBClient.client
    client.query("SELECT * FROM users ORDER BY id LIMIT 100;").to_a
  end

  def self.find_by_prefix(first_name_prefix, last_name_prefix)
    client = DBClient.client
    client.query("""
      SELECT * FROM users
      WHERE LOWER(first_name) LIKE '#{client.escape(first_name_prefix)}%' AND LOWER(last_name) LIKE '#{client.escape(last_name_prefix)}%'
      ORDER BY id LIMIT 100;
      """
    ).to_a
  end
end